﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using XtraWishesWeb.Common;
using XtraWishesWeb.Models;

namespace XtraWishesWeb.Controllers
{
    [RoutePrefix("Edit")]
    public class EditController : Controller
    {
        private static APIHEADERS apiheader = new APIHEADERS();
        private static string apiUrl = ConfigurationManager.AppSettings["localapiurl"].ToString();
        private readonly string Token = "Token";
        // GET: Edit
        [Route("~/Edit/{jsonId?}")]
        public ActionResult Index(int jsonId)
        {

            //using (HttpClient client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(apiUrl);
            //    client.DefaultRequestHeaders.Add(Token, apiheader.GenrateToken());
            //    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            //    HttpResponseMessage response = await client.GetAsync(apiUrl + "AdminApi/GetJson/?Jsonid=" + jsonId);
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var data1 = response.Content.ReadAsStringAsync().Result;
            //        var result = JsonConvert.DeserializeObject<APImodelresponsevisitorjson>(data1);
            //        //return data1;
            //        ViewBag.jsonString = result.Result.Jsonstring;
            //        ViewBag.jsonid = result.Result.jsonid;
            //    }



            //}
            ViewBag.jsonid = jsonId;
            return View();
        }
    }
}