﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using XtraWishesWeb.Models;

namespace XtraWishesWeb.Controllers
{
    public class AdminApiController : ApiController
    {
        ATMSEntities db = new ATMSEntities();

       





        [HttpGet]
        public async Task<HttpResponseMessage> GetGreetings(int? Typeid, int? SubTypid)
        {
            //return await Task.Run(() =>
            //{

            try
            {
                if (Typeid == null && SubTypid == null)
                {

                
                var result = db.Workords.Where(a => a.status == 1).ToList();
                    return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success", Result=result });
                }
                else
                {
                    if (Typeid != null && SubTypid != null)
                    {


                        var result = db.Workords.Where(a => a.status == 1 && a.Gtypeid == Typeid && a.Gsubtypeid == SubTypid).ToList();
                        return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success", Result = result });
                    }
                    else
                    {
                        if(Typeid != null )
                        {
                            var result = db.Workords.Where(a => a.status == 1 && a.Gtypeid == Typeid && a.Gsubtypeid == SubTypid).ToList();
                            return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success", Result = result });
                        }
                        else
                        {
                            var result = db.Workords.Where(a => a.status == 1 && a.Gsubtypeid == SubTypid).ToList();
                            return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success", Result = result });
                        }

                    }
                }
                //return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success" Result=result });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 101, Message = "fail" });
            }
            //});
        }


        [HttpGet]
        public async Task<HttpResponseMessage> ViewImage(int GreetingId)
        {
            try
            {

                var result = (from Wr in db.Workords join js in db.WorkorderDepartmentjs on Wr.Gjsonid equals js.jsonid where Wr.Gid == GreetingId select new { Wr.Gid, Wr.Showimgpath, js.jsonid, js.Jsonstring }).FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success", Result = result });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 101, Message = "fails" });
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetJson(int Jsonid)
        {
            try
            {

                var result = db.WorkorderDepartmentjs.Where(a => a.jsonid == Jsonid).FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success", Result = result });
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 101, Message = "fails" });
            }
        }
        [HttpGet]
        public int AddJson(string Josn)
        {
            WorkorderDepartmentj json = new WorkorderDepartmentj();
            try
            {


                json.Jsonstring = Josn;
                json.createdDate = DateTime.UtcNow;
                json.status = 1;
                db.WorkorderDepartmentjs.Add(json);
                db.SaveChanges();
                return json.jsonid;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }


        [HttpPost]
        public async Task<HttpResponseMessage> GetJson(WorkorderDepartmentj ob)
        {
            try
            {

                var result = db.WorkorderDepartmentjs.Where(a => a.jsonid == ob.jsonid).FirstOrDefault();
                result.Jsonstring = ob.Jsonstring;
                db.Entry(result).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success" });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { Code = 101, Message = "fails" });
            }
        }


        public string Saveimage(string Base64,string ImageNAme)
        {
            var path = HttpContext.Current.Server.MapPath("~/ShowImage/");
            string imagename=ImageNAme +DateTime.Now.ToString().Replace("/", "-").Replace(" ", "-").Replace(":", "")+".png";
           // var filename = DateTime.Now.ToString().Replace("/", "-").Replace(" ", "-").Replace(":", "") + ".png";
            string fileNameWitPath = path + imagename;
            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = Convert.FromBase64String(Base64);
                    bw.Write(data);
                    bw.Close();

                }
            }

            var currentpathname = Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/ShowImage/" + imagename;
            return currentpathname;

        }


        [HttpPost]
        public async Task<HttpResponseMessage> AddGr(GreetingModel obj)
        {
            //return await Task.Run(() =>
            //{

                try
                {


                    Workord GR = new Workord();
                    GR.Gjsonid = AddJson(obj.Json);
                    GR.Gtypeid = obj.Gtypeid;
                    GR.Gsubtypeid = obj.Gsubtypeid;
                    GR.GheadingMessage = obj.GheadingMessage;
                    GR.GDescription = obj.GDescription;
                    var path = Saveimage(obj.base64, obj.GheadingMessage);
                    GR.Showimgpath = path;
                    GR.GimgTypeid = 2;
                    GR.createdDate = DateTime.Now;
                    GR.status = 1;
                    db.Workords.Add(GR);
                    db.SaveChanges();



                    return Request.CreateResponse(HttpStatusCode.OK, new { Code = 100, Message = "Success" });
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { Code = 101, Message = "fail" });
                }
            //});
        }
    }
}
