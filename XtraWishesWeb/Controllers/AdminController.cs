﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using XtraWishesWeb.Common;
using XtraWishesWeb.Models;

namespace XtraWishesWeb.Controllers
{
    public class AdminController : Controller
    {
        private static APIHEADERS apiheader = new APIHEADERS();
        private static string apiUrl = ConfigurationManager.AppSettings["localapiurl"].ToString();
        private readonly string Token = "Token";
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddGreeting()
        {
            return View();
        }



        [HttpPost]
        public ActionResult SvaeFile(string base64)
        {
            //Convert Base64 Encoded string to Byte Array.
            var path = Server.MapPath("~/ShowImage/");
            var filename = DateTime.Now.ToString().Replace("/", "-").Replace(" ", "-").Replace(":", "") + ".png";
            string fileNameWitPath = path + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "-").Replace(":", "") + ".png";
            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                 {
                    byte[] data = Convert.FromBase64String(base64);
                    bw.Write(data);
                    bw.Close();

                  }
           }

            var currentpathname = Request.Url.GetLeftPart(UriPartial.Authority) + "/ShowImage/" + filename;
            return Json(currentpathname);
          }


        [HttpPost]
        public ActionResult UploadBackground()
        {

            var file = Request.Files[0];

            var fileName = Path.GetFileName(file.FileName);

            var path = Path.Combine(Server.MapPath("~/Background/"), fileName);
            file.SaveAs(path);
            var currentpathname = Request.Url.GetLeftPart(UriPartial.Authority) + "/Background/" + fileName;
            return Json(currentpathname);
        }


        [HttpPost]
        public ActionResult Uploadimage()
        {

            var file = Request.Files[0];

            var fileName = Path.GetFileName(file.FileName);

            var path = Path.Combine(Server.MapPath("~/imges/"), fileName);
            file.SaveAs(path);

            var currentpathname = Request.Url.GetLeftPart(UriPartial.Authority) + "/imges/" + fileName;
            return Json(currentpathname);
        }

        public async Task<JsonResult> AddGreetingss(GreetingModel obj)
        {

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Add(Token, apiheader.GenrateToken());
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //HttpResponseMessage response = await client.GetAsync(apiUrl + "AdminApi/GetGreetings/?Typeid=,&SubTypid=");
                var response = await client.PostAsJsonAsync(apiUrl + "AdminApi/AddGr", obj);
                if (response.IsSuccessStatusCode)
                {
                    var data1 = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<APImodelresponsevisitor>(data1);
                    //return data1;
                    return Json(result);
                }



            }

            return Json("1014");
          
        }


        public JsonResult saveImgultofile(string Url)
        {
            var webClient = new WebClient();
            byte[] imageBytes = webClient.DownloadData(Url);
            var path = Server.MapPath("~/ShowImage/");
            var filename = DateTime.Now.ToString().Replace("/", "-").Replace(" ", "-").Replace(":", "") + ".png";
            string fileNameWitPath = path + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "-").Replace(":", "") + ".png";
            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                   // byte[] data = Convert.FromBase64String(base64);
                    bw.Write(imageBytes);
                    bw.Close();

                }
            }

            var currentpathname = Request.Url.GetLeftPart(UriPartial.Authority) + "/ShowImage/" + filename;
            return Json(currentpathname);

            return Json("");
        }

    }
}