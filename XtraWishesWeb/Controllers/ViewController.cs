﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using XtraWishesWeb.Common;
using XtraWishesWeb.Models;

namespace XtraWishesWeb.Controllers
{
    [RoutePrefix("PreView")]
    public class ViewController : Controller
    {
        private static APIHEADERS apiheader = new APIHEADERS();
        private static string apiUrl = ConfigurationManager.AppSettings["localapiurl"].ToString();
        private readonly string Token = "Token";
        // GET: View
        [Route("~/PreView/{GreetingId?}")]
        public async Task<ActionResult> Index(int GreetingId)
        {



            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Add(Token, apiheader.GenrateToken());
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl + "AdminApi/ViewImage/?GreetingId=" + GreetingId);
                if (response.IsSuccessStatusCode)
                {
                    var data1 = response.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<APImodelresponsevisitor1>(data1);
                    //return data1;
                    return View(result.Result);
                }



            }
            return View();
        }



        // [Route("/PreView/Test}")]
        //public ActionResult TestFB()
        //{
        //    return View();
        //}
    }
}