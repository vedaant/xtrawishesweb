﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XtraWishesWeb.Models
{
    public class GreetingModel
    {
        
        public int Gid { get; set; }
        public Nullable<int> Gjsonid { get; set; }
        public string Json { get; set; }
        public string base64 { get; set; }
       public Nullable<int> Gtypeid { get; set; }
        public Nullable<int> GimgTypeid { get; set; }
        public Nullable<int> Gsubtypeid { get; set; }
        public string GDescription { get; set; }
        public string GheadingMessage { get; set; }
        public string Gimgpath { get; set; }
        public string Showimgpath { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<int> status { get; set; }







    }

    public class APImodelresponsevisitor
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public List<Workord> Result { get; set; }


    }
    public class APImodelresponsevisitor1
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public Workord1 Result { get; set; }


    }
    public class APImodelresponsevisitorjson
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public WorkorderDepartmentj Result { get; set; }


    }


    public class Workord1
    {
        public int jsonid { get; set; }
        public int Gid { set; get; }
        public string Showimgpath { set; get; }
        public string Jsonstring { set; get; }
    }
}