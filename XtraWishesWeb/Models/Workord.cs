//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XtraWishesWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Workord
    {
        public int Gid { get; set; }
        public Nullable<int> Gjsonid { get; set; }
        public Nullable<int> Gtypeid { get; set; }
        public Nullable<int> GimgTypeid { get; set; }
        public Nullable<int> Gsubtypeid { get; set; }
        public string GDescription { get; set; }
        public string GheadingMessage { get; set; }
        public string Gimgpath { get; set; }
        public string Showimgpath { get; set; }
        public Nullable<int> createdby { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<int> status { get; set; }
    }
}
