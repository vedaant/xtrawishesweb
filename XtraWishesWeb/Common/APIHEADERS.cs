﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XtraWishesWeb.Common
{
   
        public class APIHEADERS
        {

            public string GenrateToken()
            {
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                byte[] key = Guid.NewGuid().ToByteArray();
                string token = Convert.ToBase64String(time.Concat(key).ToArray());
                return token;
            }

        }
    
}