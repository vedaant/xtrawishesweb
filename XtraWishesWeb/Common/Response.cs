﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XtraWishesWeb.Common
{
    public class Response<T>
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public T Result{get;set;}
        
    }
}
